﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFAFrontend.Models
{
    public class TrainingProgram
    {
        public int TrainingProgramId { get; set; }
        public string Name { get; set; }
        public string Catalog { get; set; }
        public Goal Goal { get; set; }
        public ProgramWorkout ProgramWorkout { get; set; }
    }
}
