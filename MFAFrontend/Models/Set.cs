﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFAFrontend.Models
{
    public class Set
    {
        public int SetId { get; set; }
        public int ExerciseRepetitions { get; set; }  
    }
}
