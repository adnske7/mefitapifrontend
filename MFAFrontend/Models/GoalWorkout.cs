﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFAFrontend.Models
{
    public class GoalWorkout
    {
        public int GoalWorkoutId { get; set; }
        public int EndDate { get; set; }
        public Workout Workout { get; set; }
        public int WorkoutId { get; set; }
        public Goal Goal { get; set; }
        public int GoalId { get; set; }
    }
}
