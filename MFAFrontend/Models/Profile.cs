﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MFAFrontend.Models
{
    public class Profile
    {
        public int ProfileId { get; set; }
        public Address Address { get; set; }
        public int? AddressId { get; set; }
        public Goal Goal { get; set; }
        public int? GoalId { get; set; }
        public Workout Workout { get; set; }
        public int? WorkoutId { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public string MedicalConditions { get; set; }
        public string Disabilities { get; set; }
        public User User { get; set; }
        public int? UserId { get; set; }
    }
}
