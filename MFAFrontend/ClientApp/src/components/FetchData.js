import React, { Component } from 'react';

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
    this.state = { profiles: [], loading: true };
  }

  componentDidMount() {
    this.populateProfileData();
  }

  static renderProfilesTable(profiles) {
      return (
          <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
            <tr>
            <th>Profile Id</th>
            <th>Name</th>
            <th>Height</th>
            <th>Weight</th>
            <th>Address</th>
                  <th><button className="btn primary ml-2" /* onClick={() => deleteProfile(profile.id)}*/>Create New</button></th>
          </tr>
        </thead>
        <tbody>
                {profiles.map(profile =>
                    <tr key={profile.profileId}>
                        <td>{profile.profileId}</td>
                        <td>{profile.user.firstName} {profile.user.lastName}</td>
                        <td>{profile.height}</td>
                        <td>{profile.weight}</td>
                        <td>{profile.address.addressLine1}</td>
                  
                  <td>
                      <button className="btn btn-primary ml-2" /*onClick={() => editProfile(profile)}*/>Edit</button>
                      <button className="btn primary ml-2" /* onClick={() => deleteProfile(profile.id)}*/>Delete</button>

                        </td>
                    </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
        : FetchData.renderProfilesTable(this.state.profiles);

    return (
      <div>
        <h1 id="tabelLabel" >User profiles</h1>
        <p>This component demonstrates fetching data from the server.</p>
        {contents}
      </div>
    );
  }

  async populateProfileData() {
    const response = await fetch('api/profiles');
    const data = await response.json();
    this.setState({ profiles: data, loading: false });
  }
}
